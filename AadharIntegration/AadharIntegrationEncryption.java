import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class AadharIntegrationEncryption
{
  @SuppressWarnings("null")
  public static String encrypt(final String jsonString, final String secretKey) {
    try {
      byte[] enc = null;

      byte[] arrKey = secretKey.getBytes("UTF-8");
      byte[] ivKey = secretKey.getBytes("UTF-8");

      byte[] bytIn = jsonString.getBytes();
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      cipher.init(1, new SecretKeySpec(arrKey, "AES"), new IvParameterSpec(ivKey));
      enc = cipher.doFinal(bytIn);

      return new String(Base64.getEncoder().encode(enc)).trim();
    } catch (Exception ex) {
      ex.printStackTrace();
    } return null;
  }

  public static void main(final String[] args) throws Exception {
    String encryptedText = args[0];
    String secretKey = args[1];
    encryptedText = AadharIntegrationEncryption.encrypt(encryptedText, secretKey);
    System.out.println(encryptedText);
  }
}
