import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class AadharIntegrationDecryption
{
  @SuppressWarnings("null")
  public static String decrypt(final String encryptedText, final String key) {
    try {
      String encrypted = encryptedText;
      IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));
      SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(2, skeySpec, iv);
      return new String(cipher.doFinal(Base64.getDecoder().decode(encrypted)));
    } catch (Exception ex) {
      ex.printStackTrace();
    } return null;
  }

  public static void main(final String[] args) throws Exception {
    String encryptedText = args[0];
    String secretKey = args[1];
    encryptedText = AadharIntegrationDecryption.decrypt(encryptedText, secretKey);
    System.out.println(encryptedText);
  }
}
